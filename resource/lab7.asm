org 100 ; данные
cor_block_all:  word 0000   ; инкремент, побитовый сдвиг, если блок сработал
cor_right:      word 000F   ; верны все данные
cor_all:        word 0000   ; верна команда
test_block_1:   word D584   ; отрицательное число больше -2^15
check_block_1:  word 0000   ; данные, которые будут работать с командой
test_block_2:   word 8000   ; отрицательное число равное -2^15
check_block_2:  word 0000   ; данные, которые будут работать с командой
test_block_3:   word 6548   ; положительное число больше 0
check_block_3:  word 0000   ; данные, которые будут работать с командой
test_block_4:   word 0000   ; 0
check_block_4:  word 0000   ; данные, которые будут работать с командой

org 200 ; подпрограмы
cor_block_right:    word 0000
                    cla
                    add cor_block_all
                    rol
                    inc
                    mov cor_block_all
                    br (cor_block_right)
cor_block_wrong:    word 0000
                    cla
                    add cor_block_all
                    rol
                    mov cor_block_all
                    br (cor_block_wrong)

org 3B8 ; программа
begin:  cla
prep:   add test_block_1    ; загрузка данных в рабочие ячейки
        mov check_block_1
        cla
        add test_block_2
        mov check_block_2
        cla
        add test_block_3
        mov check_block_3
        cla
        add test_block_4
        mov check_block_4
        cla
        mov cor_block_all   ; подготовка проверяющих ячеек
        mov cor_all
        br block_1
;--------------------------------------------------
block_1:    cla
	    hlt
            loop check_block_1
            br block_1_nskip
            jsr cor_block_wrong
            br block_2
block_1_nskip:  jsr cor_block_right
                br block_2
;--------------------------------------------------
block_2:    cla
            loop check_block_2
            br block_2_nskip
            jsr cor_block_right
            br block_3
block_2_nskip:  jsr cor_block_wrong
                br block_3
;--------------------------------------------------
block_3:    cla
            loop check_block_3
            br block_3_nskip
            jsr cor_block_right
            br block_4
block_3_nskip:  jsr cor_block_wrong
                br block_4
;--------------------------------------------------
block_4:    cla
            loop check_block_4
            br block_4_nskip
            jsr cor_block_wrong
            br check
block_4_nskip:  jsr cor_block_right
                br check 
;--------------------------------------------------
check:  cla
        add cor_block_all
        sub cor_right
        beq right
wrong:  cla
        mov cor_all
        br exit
right:  cla
        inc
        mov cor_all
        br exit
exit: hlt